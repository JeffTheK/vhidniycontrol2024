﻿using System.Linq;

internal class Program
{
    private static void Main(string[] args)
    {
        // Приклад

        // 1
        Console.WriteLine(Convert.ToString(169125, toBase: 2));
        Console.WriteLine($"Кількість одиниць: {CountBinaryNumberOnes(169125)}"); // 101001010010100101
        Console.WriteLine();

        // 2
        Console.WriteLine($"Скількі разів у числі зустрічаються кожна з цифр (0-9) в 1926919230");
        Console.WriteLine("Число\t- кількість");
        foreach (var kv in CountDigitOccurence(1926919230))
        {
            Console.WriteLine($"{kv.Key}\t- {kv.Value}");
        }
    }

    // У бінарному вигляді числа підраховують кількість одиниць
    public static int CountBinaryNumberOnes(int number)
    {
        string binary = Convert.ToString(number, toBase: 2);
        return binary.ToCharArray().Count(n => n == '1');
    }

    // Підраховує скількі разів у числі зустрічаються кожна з цифр (0-9)
    public static Dictionary<int, int> CountDigitOccurence(int number)
    {
        var digits = number.ToString().ToCharArray().ToList();
        var result = new Dictionary<int, int>();
        for (int i = 0; i != 10; i++)
        {
            result.Add(i, digits.Count(d => d.ToString() == i.ToString()));
        }

        return result;
    }
}